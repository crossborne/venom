hue = require('node-hue-api')
fetch = require('isomorphic-fetch');

api = null
lights = []

fetch('https://www.meethue.com/api/nupnp')
  .then (result) ->
    result.json().then (json) ->
      api = new hue.HueApi json[0].internalipaddress, 'newdeveloper'
      api.lights().then (result) ->
          result.lights.map (light) ->
              lights.push light

module.exports = (robot) ->

    lastState = null
    robot.hear /hue ([a-z]+)( ([0-9]+))?/i, (res) ->

        state = ->
            hue.lightState.create()

        states =
            'on': state().on()
            'off': state().off()

            'pink': state().hue(60000).bri(255).sat(255)
            'eventstag': state().hue(52000).bri(255).sat(255)
            'violet': state().hue(48000).bri(255).sat(255)
            'purple': state().hue(48000).bri(255).sat(255)
            'blue': state().hue(47000).bri(255).sat(255)
            'green': state().hue(25000).bri(255).sat(255)
            'yellow': state().hue(20000).bri(255).sat(255)
            'orange': state().hue(8000).bri(255).sat(255)
            'red': state().hue(0).bri(255).sat(255)
            'warm': state().white(500, 100)
            'white': state().hue(0).bri(255).sat(0)
            'loop': state().effect('colorloop')
            'noloop': state().effect('none')
            'dim': state().bri(50)

        word = res.match[1]

        if states[word]?
            lights.map (light) ->
                if res.match[3] and res.match[3] != light.id
                    return

                api.setLightState(light.id, states[word])
                    .then (result) ->
                        lastState = states[word]
                        res.send "Setting light *#{light.id}* to *#{word}*..."
        else
            res.send "Undefined state `#{word}`. Defined states: `" + Object.keys(states).join("` `") + "`"

    robot.hear /status/, (res) ->
        lights.map (light) ->
            api.getLightState(light.id, (err, result) -> res.send(JSON.stringify(result)))

    robot.hear /strobe( ([0-9]+))?( ([0-9]+))?/i, (res) ->

        strobe = (light, howManyTimes = 3) ->

            color = res.match[4] || Math.floor(Math.random() * 65535)

            stateBlue = hue.lightState.create()
                .hue(color)
                .transitiontime(0)

            stateWhite = hue.lightState.create()
                .hue(65535 - color)
                .transitiontime(0)

            api.setLightState(light.id, stateWhite)
                .then (result) ->
                    api.setLightState(light.id, stateBlue)
                        .then (result) ->
                            if howManyTimes > 0
                                strobe light, howManyTimes - 1

        howManyTimes = res.match[2] || 10

        res.send "Strobing *#{lights.length}* lights *#{howManyTimes}* times... :boom: :metal:"
        lights.map (light, i) ->
            setTimeout ->
                strobe(light, howManyTimes)
                    .then ->
                        if lastState
                            api.setLightState light.id, lastState
            , i * 50

    timer = null
    robot.hear /party( ([0-9]+))?( ([0-9]+))?/i, (res) ->

        strobe = (light, howManyTimes = 3) ->

            color = res.match[4] || Math.floor(Math.random() * 65535)

            stateColor = hue.lightState.create()
                .hue(color)
                .bri(255)
                .transitiontime(0)

            stateDark = hue.lightState.create()
                .bri(0)
                .transitiontime(0)

            api.setLightState(light.id, stateColor)
                .then (result) ->
                    api.setLightState(light.id, stateDark)
                        .then (result) ->
                            if howManyTimes > 0
                                strobe light, howManyTimes - 1

        howManyTimes = res.match[2] || 10

        lights.map (light, i) ->
            setTimeout ->
                strobe(light, howManyTimes)
                    .then ->
                        if lastState
                            api.setLightState light.id, lastState
            , i * 50

        res.send "Partyin *#{lights.length}* lights *#{howManyTimes}* times... :metal: :boom:"

    robot.hear /toggle ([0-9]+)/i, (res) ->
        lights.map (light) ->
            if light.id == res.match[1]
                api.lightStatus(light.id).then (result) ->
                    if result.state.on
                        api.setLightState light.id, hue.lightState.create().off()
                    else
                        api.setLightState light.id, hue.lightState.create().on()

    # sunrise and sunset
    controlSunset = true
    suncalcTimeout = null
    robot.hear /toggle automat/, (res) ->
        controlSunset = !controlSunset
        if !controlSunset
            res.send 'turning off auto sunrise/sunset'
            clearTimeout suncalcTimeout
        else
            res.send 'turning on auto sunrise/sunset'
            checkSun()

    SunCalc = require('suncalc')

    checkSun = ->
        times = SunCalc.getTimes(new Date(), 51.5072, 0.1275) # london
        now = new Date()
        if (Math.abs(now - times.sunset) / 1000) < 60
            state = hue.lightState.create().off()
        else if (Math.abs(now - times.sunrise) / 1000) < 60
            state = hue.lightState.create().on()

        lights.map (light) ->
            api.setLightState(light.id, state)

        suncalcTimeout = setTimeout checkSun, 60000
    checkSun()


    # game
    gameStarted = false
    enteringNames = false
    sender = null
    timer = null

    robot.brain.set 'players', []

    robot.hear /partygoat$/i, (msg) ->

        sender = msg.message.user.name
        msg.reply "Right #{msg.message.user.name}, so who is playin?"
        gameStarted = true
        setTimeout ->
            enteringNames = true
        , 3000

    shuffle = (a) ->
        i = a.length
        while --i > 0
            j = ~~(Math.random() * (i + 1))
            t = a[j]
            a[j] = a[i]
            a[i] = t
        a

    robot.hear /(shuffle|players)$/i, (msg) ->
        players = robot.brain.get 'players'
        if msg.match[1] is "shuffle"
            robot.brain.set 'players', shuffle(players)

        msg.send "PLAYERS: #{players}"

    robot.hear /([a-z, 0-9]+)/i, (msg) ->

        if enteringNames and msg.message.user.name is sender
            enteringNames = false
            names = msg.match[1].split(',')
            names = names.map (name) ->
                name = name.trim()
                name = name[0].toUpperCase() + name.substr(1).toLowerCase()
                return name

            robot.brain.set 'players', names

            msg.send "PLAYERS: #{names}"

    robot.hear /add player ([a-z 0-9]+)$/i, (msg) ->
        names = robot.brain.get 'players'
        name = msg.match[1].trim()
        name = name[0].toUpperCase() + name.substr(1).toLowerCase()
        names.push name

        robot.brain.set 'players', names
        msg.send "ok added #{name}"

    robot.hear /start goat(( [0-9]+))$/i, (msg) ->

        gameInterval = msg.match[2] * 1000 || 1000 * 60 * 10

        names = robot.brain.get 'players'
        robot.brain.set 'playingPlayers', names

        shuffled = shuffle names

        icons = [
            'heavy petting'
            'swap an article of clothing with opposite sex'
            'hold hand with partner until you both finish your drink'
            'you have to take a shot together'
            'turn on your favorite dance song'
            'flash roman on toilet'
            'get fucked'
            'kiss on cheek + photo'
            "one person has to call the other's mom"
            'call michael czolko = ask roman for skype'
        ]

        icons = shuffle icons

        singleThings = [
            'take a selfie with the :goat:'
            'take a selfie with Harry :camera:'
            'have a :beer:'
            'you get to pick a song :musical_score:'
        ]

        singleThings = shuffle singleThings

        for name, i in shuffled by 2
            secondName = names[i + 1]

            do (name, secondName, i) ->
                setTimeout ->
                    api.setGroupLightState 0, hue.lightState.create().alertLong()

                    if secondName?
                        icon = icons.shift()
                        msg.send "#{name} + #{secondName}: #{icon} (remember, if you dont want to do this, just have a shot! :))"
                    else
                        message = singleThings.shift()
                        msg.send "#{name} #{message}"
                , gameInterval * i + gameInterval

        setTimeout ->
            msg.send "Game end"
        , gameInterval * shuffled.length / 2 + gameInterval
